# Manuálové stránky v češtině

Toto je projekt, jehož účelem je vytvořit sbírku neoficiálních (nevydaných
autorem softwaru, který popisují) manuálových stránek, které je možné
nainstalovat jako balíček.

Manuálové stránky jsou psané ve formátu podobném Markdownu.

Manuálové stránky naleznete v adresáři [manpages/](manpages/). Pro každý program je zde podadresář.

Pokud chcete přidat manuálovou stránku, pročtěte si, prosím, [stránku o formátu](https://gitlab.com/man-cs/man-cs/wikis/Form%C3%A1t-manu%C3%A1lov%C3%BDch-str%C3%A1nek) na [Wiki](https://gitlab.com/man-cs/man-cs/wikis/) a nějaké existující manuálové stránky.